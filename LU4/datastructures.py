# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 19:31:50 2018

@author: jonas
"""

APPL = 100
print ("stock price is", APPL)
#Dictionary
a_dict = {'APPL': 100}
print ("stock price is", a_dict['APPL'])
#               KEY    VALUE
#stock_prices = {"APPL":100}
#print(type())

#Exercise 0
#index of c = 2, a = 0, b = 1
my_tuple = ('a', 'b', 'c')
print(my_tuple[0])

#Exercise 1
tup = (1,2,3)
#tup[0] = 5 #TypeError: 'tuple' object does not support item assignment

#Exercise 2
appl_prices = (84,91,135,132,101)
print (appl_prices[0:4]) #whole tuple with brackets
print (appl_prices[1]) #single part of the tuple called by position

#Exercise 3
#since it does not support the add/sort function
appl_prices = (84,91,135,132,101)
print (appl_prices[0:4])
print (appl_prices.count(123)) #does not show .short/.add
print (appl_prices[0:5])
#Change tuple into a list with loops and write one value into the other one

#Exercise 4
appl_prices = [231,123,454,342,267]
print(appl_prices)
appl_prices.append(145)
print(appl_prices)

#Exercise 5
#tuple cant be manipulated - list can

#Exercise 6
tuple1 = (1,2,3)
tuple2 = (1,2,3)
tuple3 = (2,1,3)
list1 = [1,2,3]
#remove value in list
print(list1[0:2])
list1.remove(3) #removes value 3 independet of position within the list
print(list1)

print(tuple1 == tuple2)
print(tuple1 == tuple3) # Evidence that it does not refer to the type 
#but to the values themselves
print(list1 == tuple2) #even though values are the same - the type is 
#different so program prints "False"

#Exercise 7
tuple1 = (1,2,3)
print(tuple1[0:3])
print(tuple1[1:3])
list1 = list(tuple1)
print(list1)
list1.append(0.6)
list1.append(5)
print(list1)
list1.sort(reverse=True)
print(list1)

#Exercise 8
dictionary1 = {"Appl": 31, "Sams": 41, "Amaz": 314}
print(dictionary1)
dictionary1 ["Appl"] = 54
print(dictionary1)
print(dictionary1.keys()) # gives only the keys
print(dictionary1.values()) #gives only values
print(dictionary1.items()) #gives both
print(dictionary1) # not equal to print dictionary1.items()

#Exercise 9
dic = {}
print(dic)
dic["Daim"] = 101
dic["BMW"] = 89
dic["Audi"] = 122
print(dic)
print(dic.items())
dic["hello"] = "world"
print(dic["hello"])
dic["Introduction"] = "Programming"
print(dic.items())

#Exercise 10
stock_prices = {"Appl": 100, "Goog": 91}
stock_of_interest = "Appl"
print(stock_prices[stock_of_interest]) #prints out 100 so basically the 
# value that is assigned to the key without anything else

#Exercise 11
create_list_1 = [11,65,23,1,57,99]
print(create_list_1)
create_list_2 = []
for x in range (6):
    create_list_2.append(x)

print(create_list_2)

#Exercise 12
dic = {"Appl": 31, "Sams": 41, "Amaz": 314}
print(dic)
del dic["Appl"]
print(dic.items())
dic.pop("Sams", None)
print(dic.items())
dic.pop("Sams", 234)
print(dic.items())
'''
pop(key[, default])
If key is in the dictionary, remove it and return its value, else return 
default. If default is not given and key is not in the dictionary, a 
KeyError is raised
'''

#Exercise 13
def list_taker(a):
    a.append("homie")
    print(a)
    return a

a_list = []
list_taker(a_list)
list_taker(a_list)
print(a_list)

#Exercise 14
def ave(thing):
    sum_1 = thing[0]+thing[1]+thing[2]+thing[3]+thing[4]+thing[5]+thing[6]
    local_average = sum_1 / 7
    return local_average

list_1 = [321,654,787,231,578,768,324]
global_average = ave(list_1)
print(global_average)
