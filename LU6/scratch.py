# -*- coding: utf-8 -*-
#scratch.py is place to run stuff
#other files are used to define stuff

# link on computer: C:\Users\jonas\.spyder-py3\intro-programming

from primate import gorilla
#import of certain function - no name needed anymore
from canine.dog import speak #to add more: ", be_mad, hungry, *etc*"


gorilla.speak()

#no dog needed anymore
speak()


#Exercise 1
from feline.cat import speak
import feline.cat.speak
