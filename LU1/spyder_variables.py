# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 18:04:42 2018
Spider IDE integrated development environment
@author: jonas
"""
#exercise 1
favourite_stock = "Tesla"
value_favourite_stock = 74.1
number_of_shares = 51
action = "buy"

print('I would like to', action, 
      number_of_shares, 'shares in', 
      favourite_stock, '.')

#exercise 2
person_1 = "Tim"
person_2 = "Paul"
amount_of_money = 10
amount_of_stocks = 2
company_name = "Tesla"
print(person_1, 'needs to borrow', amount_of_money, 
      'euros from', person_2, 'in order to trade', 
      amount_of_stocks, 'stocks in', company_name + '.')

#exercise 3
number_1 = 4
number_2 = 2
number_3 = number_1 * number_2
print(number_3)

#exercise 4
celcius = 30
fahrenheit = celcius * 9/5 +32
kelvin = celcius + 273.15
print(fahrenheit)
print(round(kelvin))

