# -*- coding: utf-8 -*-
"""
Created on Tue Feb 27 18:08:49 2018

@author: jonas
"""

#Examples - Boolean
if True: #everything is initially True
    print("Hello World")
if False: #death code - it will never run because everything is true
    print("Goodbye World")
    
a = True
if a:
    print("Hello", a)
    
b = False
if b:
    print("Hello", b)
    
    
    
#Examples - Other types
    
if 1:
    print("Hello", 1)
    
if 0:
    print("Hello", 0) #does not print
    
if 2:
    print("Hello", 2)
    
if "world":
    print("Hello", "World")
    
if"":
    print("Hello", "") #does not print
###################################NEW#################################
if True: 
    print("Hello World")
    
def return_true():
    return True

if return_true():
    print("Hello World")

val = return_true()

if val:
    print("Hello World") 
###################################NEW#################################
a = True
b = False
if a or a:
    print("at least one thing is True")
if a or b:
    print("at least one thing is True")
if b or b:
    print("at least one thing is True")
if b or b or b or a:
    print("at least one thing is True")
###################################NEW#################################
'''ticker_value = "Amaz"
if ticker_value == "Appl":
    #do sth
elif ticker_value == "Goog":
    #do sth
else: # default behaviour - maybe dont do anything
    #dont do anything'''
###################################NEW#################################
a_list = [1,2,3]

for x in a_list:
    print(x)
###################################NEW#################################
#Exercise 1    
'''1: True
1.1 True
0 False
0.00001 True
"" False
'''
if None:
    print("True")
else:
    print("False")
#None has boolean value False
    
#Exercise 2
def takes (input_sth):
    if input_sth == "APPL":
        return True
    else: 
        return False

print(takes("GOOG"))
print(takes("APPL"))

#Exercise 3
def gender(my_gender):
    if my_gender == "Female":
        return "You like pink"
    elif my_gender == "Male":
        return "You hate pink"
    else:
        return "Nope"
    
print(gender("Male"))
print(gender("Female"))
print(gender("-"))


#Exercise 4
'''
The line is in fact being printed because it is still in the scope of the 
function. We need to introduce a return statement after the if statements
respectively inside each if statement to exit the function if there is never
another gender entered than this.
'''
#Exercise 5
def compare(age_mother = 57, age_father = 59):
    if age_mother == age_father:
        return age_mother + age_father
    else:
        if age_father > age_mother:
            return age_father - age_mother
        elif age_mother > age_father:
            return age_mother - age_father

print(compare())
print(compare(67,54))    
    
#Exercise 6
my_list = [1, 3, 7, 9]
my_dict = {"Obj1": "One", "Obj2": "Two", "Obj3": "Three", "Obj4": "Four"}

def compare (one, two):
    if len(my_list) == len(my_dict):
        print("Awesome")
    elif len(my_list) == len(my_dict):
        print ("That is sad")
        
compare(my_list, my_dict)

#Expercise 7

stock_dict = {"APPL": 115, "GOOG": 321, "FACE": 213}
def loop_key (one):
    for key in one.keys(): 
        print (key)

def loop_val (one = stock_dict):
    for y in one.values(): 
        print (y)
        
def loop_eve (dict_local):
    for key in dict_local:         
        print ("The stock price of Company", key, "is", dict_local[key])
        
loop_key(stock_dict)
loop_val()
loop_eve(stock_dict)


#Exercise 8
tuple_one = (10,20,30)
list_one = [40,50,60] #list = []
dict_one = {"APPL": 100, "GOOG": 90, "FB": 80}

def tuple_loop (one = tuple_one):
    for x in one:
        print(x)

def list_loop (one):
    for x in one:
        print (x)

def dict_loop (one = dict_one):
    for x in one.values(): #.items() / .keys()
        print(x)
    
tuple_loop()
list_loop(list_one)
dict_loop()

#Exercise 9
start_list = [4,7,1,6,8,9]
def multi (double):
    numbers = len(double)
    print(numbers)
    
    new_list = []
    for x in double:
        new_list.append(x * 2)
        print (new_list)
 
multi(start_list)


#Exercise 10
basic_dict = {"APPL": 100, "GOOG": 90, "FB": 80}

def mapping (in_dict = basic_dict):
    new_dict = {}
    
    for key, value in in_dict.items():
        new_dict.update({key: value * 2})
        
    print(new_dict)
    
mapping()

#Exercise 11
def comparison (in_list, in_num):
    len_in_list = len(in_list)
    if len_in_list > in_num:
        return True
    else:
        return False
    
my_list = [1,7,32,8]
my_num = 3
print(comparison(my_list ,my_num))
    
    