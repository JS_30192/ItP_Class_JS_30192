# -*- coding: utf-8 -*-
"""
Created on Tue Mar  6 17:56:31 2018

@author: jonas
"""

# Quiz 4

# PLEASE READ THE INSTRUCTIONS:

# BE EXTREMELY CAREFUL WITH THE NAMES OF THE FUNCTIONS!!!!
# Your functions should have exactly the same names as the ones 
# on the description!

# If you don't name the functions well, your code will not pass the tests! 

# Check the first example to understand exactly how to do it.

# EXERCISE ONE
# Write a function called stock_fun that takes a single argument. This function should
# return True if the value of the argument is 'GOOGLE' or 'TSLA'.
# Return False otherwise

def stock_fun(arg):
    if arg == "GOOGLE" or arg == "TSLA":
        return True
    else:
        return False
    
arg_global = "GOOGLE"
print(stock_fun(arg_global))   
    
pass



# EXERCISE TWO
# Write a function called comparing_ages that takes two arguments: 
# The first argument should be your age 
# The second should be the age of your mother. 

# The function should Return True if the difference between your age and your 
# mother's is bigger than 20. Return False otherwise.
    
def comparing_ages(my_age, mum_age):
    if mum_age - my_age > 20:
        return True
    else:
        return False
    
my_age_global = 26
mum_age_global = 56

print(comparing_ages(my_age_global, mum_age_global))

# EXERCISE THREE
# Write a function called list_slice that takes a list as an argument and returns the second 
# element of that list. Define a list [1,2,3] as the default value for your argument 
my_list = [1,2,3]


def list_slice (list_inside):
    return list_inside [2]

print(list_slice(my_list))


# EXERCISE FOUR
# Write a function called power_list that take a list as an argument. This function, should call 
# the list_slice() with the same argument and return the same value divided 
# by 2. 
    
# Hint: The argument you pass on power_list is the same you pass on list_slice()

def power_list (my_list):
    new_number = list_slice(my_list)
    return new_number / 2



# EXERCISE FIVE
# Write a function called list_and_number that takes two arguments: 
# The first is a single number
# The second is list 
    
# If this number is greater than 10, append that number to the list and return 
# the new list. Otherwise return the number. 

def list_and_number(num, list_inside):
    if num > 10:
        list_inside.

# EXERCISE SIX
# Write a function called compare_list that takes two arguments as its input.
    # The first argument should be a list
    # The second argument should be a number
# The function should return True if the lenght of the list is greater 
# than the number that was passed in as an argument. Return false otherwise


# EXERCISE SEVEN
# Write a function called double_dict that takes a single dictionary as an argument that maps a 
# string to a number. The function should return a new dictionary that multiplies
# each value of the input dictionary by 2


# EXERCISE EIGHT
# Consider the follwing dictionary
#stock_dict = {'AAPL':100, 'GOOG':800}

# Write a function called return_value that takes two arguments: 
    # the first one is a variable with the ticker of a stock, by default should be 'AAPL'
    # the second is a dictionary, by default should be stock_dict
    
# The variable with the ticker of the stock it's a key to the dictionary. The function
# should return the value of this key