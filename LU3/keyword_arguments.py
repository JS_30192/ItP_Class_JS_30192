# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 18:03:53 2018

@author: jonas
"""

#AKA Keywords Arguments
def print_trade (ticker_symbol='AAPL', num_stocks=5):
    print('ricardo wants to trade', num_stocks, 'of', ticker_symbol)
    
print_trade()
print_trade(num_stocks=10)
print_trade(ticker_symbol='GooG')
print_trade(num_stocks=10, ticker_symbol='SBE')
print_trade(ticker_symbol='SBE', num_stocks=10)
'''
#Exercise 1
scope: is the actual part that have influence on the program
local scope: is the part that influences the function
global scope: is the part that influences all the local scopes


#Exercise 2
variable:
    first_var = global
    second_var = global
    who = local in hello_world-function
    who_again = local in hello_other_world-function

#Exercise 3
first_var is initialized after the function wants to call it
'''
#Exercise 4
var_one = 4
def no_arguments():
    multiplied_var = var_one * 2
    return multiplied_var

modified_var = no_arguments()
print(modified_var, ' ', var_one)

'''
#Exercise 5
error because variable _power_ is defined in local part and called/printed in
global part of the programm
it returns 2 by the power of 10

#Exercise 6
Outputs:
10
10
4

power is global first
in the function the global variable power becomes global and changed to 2.
power is 2 in the computation in the nth_power-function.
powered gets assigned with the return-value of the nth_power-function.
the global variable power gets printed again.
when the programm calls the function.

#Exercise 7
printed first line: oranges banana
printed second line: coins, chocolates

#Exercise 8
Scope is the part that influences the program
local scope influences the function
global scope influences everything within the file
'''

#Exercise 9
def names (first = "Jonas", second = "Schweigert"):
    print (first, " ", second)
    return None

names()
#names(first, second) doesnt work since first and second are not defined
names(first = "Peter", second = "Typ")
names(second = "Gäbelein", first = "Sascha")
names("Felix", "Vemmer")


#Exercise 10
'''
1. line: Sam Hopkins
2. line: Ricardo Pereira
3. line: Jose Maria


#Exercise 11
def greetings (message = "Good morning", name = "Ricardo"):
    print(message, name)
    
result = greetings()
#prints None since return of function is from None-type
print(result)
print(greetings(message = "Nothing", name = "John"))
'''

def print_sth(name_local = "", message = "Good morning"):
    print(message, name_local)
    return

name_global = "John Snow"
message = "You know nothing"
print_sth()
print_sth("John Snow")
print_sth(name_global, message)
